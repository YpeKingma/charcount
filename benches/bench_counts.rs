use criterion::{criterion_group, criterion_main, Criterion};

use charcount::count_mod::{count_low_u8_bits, count_low_u8_bits_iters_simd, ps_bytes};

pub fn bench_count_chars(c: &mut Criterion) {
    for two_pow in 16..24 {
        let data_len = 1usize << two_pow;
        let mut group = c.benchmark_group(format!("throughput-{data_len}"));
        group.throughput(criterion::Throughput::Bytes(data_len as u64));
        let bytes = ps_bytes(data_len);
        group.bench_function("count_low_u8_bits", |b| {
            b.iter(|| count_low_u8_bits(&bytes))
        });
        group.bench_function("count_low_u8_bits_iters_simd", |b| {
            b.iter(|| count_low_u8_bits_iters_simd(&bytes))
        });
    }
}

criterion_group!(benches, bench_count_chars);
criterion_main!(benches);
