#![feature(portable_simd)]

use charcount::count_mod::{count_low_u8_bits, count_low_u8_bits_iters_simd, ps_bytes};

fn main() {
    const DATA_U8_LEN: usize = 1_310_000;
    let bytes = ps_bytes(DATA_U8_LEN);

    let start = std::time::Instant::now();
    let res = count_low_u8_bits(&bytes);
    let duration = start.elapsed();
    println!("count_low_u8_bits from bytes {res}",);
    println!(
        "{duration:?} for {} u8s, {} GiB/s",
        bytes.len(),
        (bytes.len() as f64) / (duration.as_nanos() as f64)
    );

    let start = std::time::Instant::now();
    let res = count_low_u8_bits_iters_simd(&bytes);
    let duration = start.elapsed();
    println!("count_low_u8_bits_iters_simd from bytes {res}",);
    println!(
        "{duration:?} for {} u8s, {} GiB/s",
        bytes.len(),
        (bytes.len() as f64) / (duration.as_nanos() as f64)
    );
}
