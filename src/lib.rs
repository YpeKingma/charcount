#![feature(portable_simd)]

pub mod count_mod {
    use core::simd::u8x64;
    use rand;
    use std::ops::BitAnd;

    fn aligned_len<T>(bytes: &[u8]) -> Result<usize, String> {
        // Ok when bytes points to consecutive properly initialized values of type T
        let ptr = bytes.as_ptr();
        let size_of_type_t = core::mem::size_of::<T>();
        if ptr.align_offset(size_of_type_t) != 0 {
            Err("start not aligned".to_owned())
        } else {
            let input_len = bytes.len();
            let output_len = input_len / size_of_type_t;
            if input_len != output_len * size_of_type_t {
                Err("end not aligned".to_owned())
            } else {
                Ok(output_len)
            }
        }
    }

    pub fn try_as_slice<T>(bytes: &[u8]) -> Result<&[T], String> {
        let output_len = aligned_len::<T>(bytes)?;
        Ok(unsafe { core::slice::from_raw_parts(bytes.as_ptr() as *const T, output_len) })
    }

    type OptionRefSlice<'a, T> = Option<&'a [T]>;

    pub fn split_as_slice<T>(
        bytes: &[u8],
    ) -> (OptionRefSlice<u8>, OptionRefSlice<T>, OptionRefSlice<u8>) {
        let ptr = bytes.as_ptr();
        let size_of_type_t = core::mem::size_of::<T>();
        let start_offset = ptr.align_offset(size_of_type_t);

        let non_aligned_before: OptionRefSlice<u8> = if start_offset == 0 {
            None
        } else if start_offset <= bytes.len() {
            Some(&bytes[..start_offset])
        } else {
            Some(bytes)
        };

        let (aligned_type_t_slice, end_offset): (OptionRefSlice<T>, usize) =
            if start_offset > bytes.len() {
                (None, start_offset)
            } else {
                let mid_len = bytes[start_offset..].len();
                let output_len = mid_len / size_of_type_t;
                if output_len == 0 {
                    (None, start_offset)
                } else {
                    (
                        Some(unsafe {
                            core::slice::from_raw_parts(
                                bytes[start_offset..].as_ptr() as *const T,
                                output_len,
                            )
                        }),
                        start_offset + output_len * size_of_type_t,
                    )
                }
            };

        let non_aligned_after: OptionRefSlice<u8> = if end_offset < bytes.len() {
            Some(&bytes[end_offset..])
        } else {
            None
        };
        (non_aligned_before, aligned_type_t_slice, non_aligned_after)
    }

    fn _try_as_slice_mut<T>(bytes: &mut [u8]) -> Result<&mut [T], String> {
        // From the from_raw_parts_mut docs:
        // The memory referenced by the returned slice must not be accessed
        // through any other pointer (not derived from the return value) for the duration of lifetime 'a.
        // Both read and write accesses are forbidden.
        let output_len = aligned_len::<T>(bytes)?;
        Ok(unsafe { core::slice::from_raw_parts_mut(bytes.as_mut_ptr() as *mut T, output_len) })
    }

    pub fn count_low_u8_bits(data: &[u8]) -> u32 {
        let (non_aligned_start, aligned_bytes_u64, non_aligned_end) = split_as_slice::<u64>(data);
        let mut res: u32 = 0;

        if let Some(start_bytes) = non_aligned_start {
            res += start_bytes.iter().map(|&b| (b & 1) as u32).sum::<u32>();
        }

        if let Some(bytes_u64) = aligned_bytes_u64 {
            const L8: u64 = 0x0101010101010101;
            res += bytes_u64
                .iter()
                .map(|&u| (u & L8).count_ones())
                .sum::<u32>();
        }

        if let Some(end_bytes) = non_aligned_end {
            res += end_bytes.iter().map(|&b| (b & 1) as u32).sum::<u32>();
        }

        res
    }

    pub fn count_low_u8_bits_iters_simd(data: &[u8]) -> u32 {
        type SimdBytes = u8x64;
        const N_LANES: usize = 64;
        const PARTIAL_SUM_LENGTH: usize = 192;
        let reduce_sum = {
            |simd_bytes: &SimdBytes| simd_bytes.as_array().iter().map(|b| *b as u32).sum::<u32>()
        };
        let l8_simd = SimdBytes::splat(1);
        let mut iter_ce = data.chunks_exact(PARTIAL_SUM_LENGTH * N_LANES);
        let mut res = 0;
        res += (&mut iter_ce)
            .map(|chunk_psum| {
                reduce_sum(
                    &chunk_psum
                        .chunks(N_LANES)
                        .map(|chunk_lane| SimdBytes::from_slice(chunk_lane).bitand(l8_simd))
                        .sum::<SimdBytes>(),
                )
            })
            .sum::<u32>();
        let mut iter_ce_rem = iter_ce.remainder().chunks_exact(N_LANES);
        res += reduce_sum(
            &(&mut iter_ce_rem)
                .map(|chunk_lane| SimdBytes::from_slice(chunk_lane).bitand(l8_simd))
                .sum::<SimdBytes>(),
        );
        res += iter_ce_rem
            .remainder()
            .iter()
            .map(|&b| b as u32 & 1)
            .sum::<u32>();
        res
    }

    pub fn ps_bytes(data_len: usize) -> Vec<u8> {
        let mut data = Vec::with_capacity(data_len);
        data.resize_with(
            data_len,
            || {
                if rand::random::<bool>() {
                    b'p'
                } else {
                    b's'
                }
            },
        );
        data
    }
}
